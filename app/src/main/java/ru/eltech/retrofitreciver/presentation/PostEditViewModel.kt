package ru.eltech.retrofitreciver.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.eltech.retrofitreciver.data.network.ApiFactory
import ru.eltech.retrofitreciver.data.network.model.PostEditBodyDto
import ru.eltech.retrofitreciver.data.network.model.PostEditResponseDto
import ru.eltech.retrofitreciver.data.network.model.PostItemDto

class PostEditViewModel : ViewModel() {

    private val _post = MutableLiveData<PostItemDto>()
    val post: LiveData<PostItemDto>
        get() = _post

    private val _response = MutableLiveData<PostEditResponseDto>()
    val response: LiveData<PostEditResponseDto>
        get() = _response

    private val api = ApiFactory.apiService

    fun getPost(postId: Int) {
        viewModelScope.launch {
            val post = api.getPostById(postId)
            _post.postValue(post)
        }
    }

    fun editPost(title: String, body: String) {
        val post = this.post.value ?: throw RuntimeException("Post == null")
        viewModelScope.launch {
            val response = api.editPost(PostEditBodyDto(
                id = post.id,
                userId = post.userId,
                title = title,
                body = body
            ))
            _response.postValue(response)
        }
    }
}