package ru.eltech.retrofitreciver.presentation

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import ru.eltech.retrofitreciver.databinding.ActivityMainBinding
import ru.eltech.retrofitreciver.presentation.adapters.PostItemAdapter

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: PostItemAdapter

    private val viewModel by lazy {
        ViewModelProvider(this)[MainViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.e("MainActivity", "onCreate")
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRV()
    }

    private fun setupRV() {
        adapter = PostItemAdapter()
        binding.rvMain.adapter = adapter
        viewModel.postList.observe(this) {
            adapter.submitList(it)
        }
        viewModel.getPostList()
        setupOnPostClickListener()
    }

    private fun setupOnPostClickListener() {
        adapter.onPostClickListener = PostItemAdapter.OnPostClickListener {
            val intent = PostEditActivity.getIntent(this, it.id)
            startActivity(intent)
        }
    }
}