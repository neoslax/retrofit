package ru.eltech.retrofitreciver.data.network.model

data class PostEditBodyDto(
    val id: Int,
    val userId: Int,
    val title: String,
    val body: String
)